json.extract! coffee_drink, :id, :name, :recipe, :cost, :picture, :created_at, :updated_at
json.url coffee_drink_url(coffee_drink, format: :json)
