class CoffeeDrinksController < ApplicationController
  before_action :set_coffee_drink, only: %i[ show edit update destroy ]

  # GET /coffee_drinks or /coffee_drinks.json
  def index
    @coffee_drinks = CoffeeDrink.all
  end

  # GET /coffee_drinks/1 or /coffee_drinks/1.json
  def show
  end

  # GET /coffee_drinks/new
  def new
    @coffee_drink = CoffeeDrink.new
  end

  # GET /coffee_drinks/1/edit
  def edit
  end

  # POST /coffee_drinks or /coffee_drinks.json
  def create
    @coffee_drink = CoffeeDrink.new(coffee_drink_params)

    respond_to do |format|
      if @coffee_drink.save
        format.html { redirect_to coffee_drink_url(@coffee_drink), notice: "Coffee drink was successfully created." }
        format.json { render :show, status: :created, location: @coffee_drink }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @coffee_drink.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /coffee_drinks/1 or /coffee_drinks/1.json
  def update
    respond_to do |format|
      if @coffee_drink.update(coffee_drink_params)
        format.html { redirect_to coffee_drink_url(@coffee_drink), notice: "Coffee drink was successfully updated." }
        format.json { render :show, status: :ok, location: @coffee_drink }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @coffee_drink.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /coffee_drinks/1 or /coffee_drinks/1.json
  def destroy
    @coffee_drink.destroy

    respond_to do |format|
      format.html { redirect_to coffee_drinks_url, notice: "Coffee drink was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_coffee_drink
      @coffee_drink = CoffeeDrink.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def coffee_drink_params
      params.require(:coffee_drink).permit(:name, :recipe, :cost, :picture)
    end
end
