# README

So far, this app uses an elegant Bootstrap theme and allows for CRUD of coffee drinks and displays the coffee drinks on the front page. Images are stored in Active Storage locally.

TODO:
- [ ] Set up CI/CD
- [ ] Set up shopping cart logic [similar to this](https://medium.com/@yxp010/simple-shopping-cart-in-rails-ece6f51b27e6)
- [ ] Add authentication to update database
- [ ] Add auth for customers, who can have carts that have drinks


