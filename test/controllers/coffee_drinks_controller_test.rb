require "test_helper"

class CoffeeDrinksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @coffee_drink = coffee_drinks(:one)
  end

  test "should get index" do
    get coffee_drinks_url
    assert_response :success
  end

  test "should get new" do
    get new_coffee_drink_url
    assert_response :success
  end

  test "should create coffee_drink" do
    assert_difference('CoffeeDrink.count') do
      post coffee_drinks_url, params: { coffee_drink: { cost: @coffee_drink.cost, name: @coffee_drink.name, recipe: @coffee_drink.recipe } }
    end

    assert_redirected_to coffee_drink_url(CoffeeDrink.last)
  end

  test "should show coffee_drink" do
    get coffee_drink_url(@coffee_drink)
    assert_response :success
  end

  test "should get edit" do
    get edit_coffee_drink_url(@coffee_drink)
    assert_response :success
  end

  test "should update coffee_drink" do
    patch coffee_drink_url(@coffee_drink), params: { coffee_drink: { cost: @coffee_drink.cost, name: @coffee_drink.name, recipe: @coffee_drink.recipe } }
    assert_redirected_to coffee_drink_url(@coffee_drink)
  end

  test "should destroy coffee_drink" do
    assert_difference('CoffeeDrink.count', -1) do
      delete coffee_drink_url(@coffee_drink)
    end

    assert_redirected_to coffee_drinks_url
  end
end
