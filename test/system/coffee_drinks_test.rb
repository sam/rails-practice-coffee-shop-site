require "application_system_test_case"

class CoffeeDrinksTest < ApplicationSystemTestCase
  setup do
    @coffee_drink = coffee_drinks(:one)
  end

  test "visiting the index" do
    visit coffee_drinks_url
    assert_selector "h1", text: "Coffee Drinks"
  end

  test "creating a Coffee drink" do
    visit coffee_drinks_url
    click_on "New Coffee Drink"

    fill_in "Cost", with: @coffee_drink.cost
    fill_in "Name", with: @coffee_drink.name
    fill_in "Recipe", with: @coffee_drink.recipe
    click_on "Create Coffee drink"

    assert_text "Coffee drink was successfully created"
    click_on "Back"
  end

  test "updating a Coffee drink" do
    visit coffee_drinks_url
    click_on "Edit", match: :first

    fill_in "Cost", with: @coffee_drink.cost
    fill_in "Name", with: @coffee_drink.name
    fill_in "Recipe", with: @coffee_drink.recipe
    click_on "Update Coffee drink"

    assert_text "Coffee drink was successfully updated"
    click_on "Back"
  end

  test "destroying a Coffee drink" do
    visit coffee_drinks_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Coffee drink was successfully destroyed"
  end
end
