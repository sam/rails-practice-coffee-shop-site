class CreateCoffeeDrinks < ActiveRecord::Migration[6.1]
  def change
    create_table :coffee_drinks do |t|
      t.string :name
      t.string :recipe
      t.float :cost

      t.timestamps
    end
  end
end
