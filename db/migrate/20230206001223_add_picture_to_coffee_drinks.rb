class AddPictureToCoffeeDrinks < ActiveRecord::Migration[6.1]
  def change
    add_column :coffee_drinks, picture, :image
  end
end
